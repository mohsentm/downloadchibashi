<?php

/*
Plugin Name: Download Status
Plugin URI: downloadchibashi.com
Description: View statistics of successful and unsuccessful downloads
Version: 1.3
Author: Mohsen hosseini
Author URI:  http://mohsenhosseini.info
*/

// update plugin library
require 'lib/update-checker/plugin-update-checker.php';
$className = PucFactory::getLatestClassVersion('PucGitHubChecker');
$myUpdateChecker = new $className(
	'https://gitlab.com/mohsentm/downloadchibashi.git',
	__FILE__,
	'master'
);

require_once( 'tab_class.php' );


class Download_Status_Plugin {

	// class instance
	static $instance;

	// download WP_List_Table object
	public $downloads_obj;

	// class constructor
	public function __construct() {
		add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
		add_action( 'admin_menu', [ $this, 'plugin_menu' ] );
	}


	public static function set_screen( $status, $option, $value ) {
		return $value;
	}

	public function plugin_menu() {

		$hook = add_menu_page(
			'آمار دانلود',
			'آمار دانلود',
			'manage_options',
			'download_status',
			[ $this, 'plugin_settings_page' ]
		);
		add_action( "load-$hook", [ $this, 'screen_option' ] );

	}
    
/*
تعداد دانلود های موفق به تفکیک روز
 select count(*),f_datetime from  wp_downloadchibashi_files_list group by DATE(f_datetime)
تعداد دانلود های ناموفق با تفکیک روز
select count(*),f_datetime  from  wp_downloadchibashi_files_list where f_fail =1 group by DATE(f_datetime);
*/
    private function user_download_status()
    {
		global $wpdb;

		$sql = "select count(*) from  {$wpdb->prefix}downloadchibashi_files_list where f_fail =1";

		$result['fail'] = $wpdb->get_var( $sql );

        $sql = 'select count(*) from  '.$wpdb->prefix.'downloadchibashi_files_list where f_fail IS NULL';

		$result['success'] = $wpdb->get_var( $sql );

        return $result;
    }
	private	function top_user_download_ip($date=NULL , $status=NULL , $limit=5 )
	{
		global $wpdb;
		$sql = "SELECT count( * ) AS Count , ip FROM {$wpdb->prefix}downloadchibashi_files_list ";
				
		if($status == 'success')
			$sql .= "WHERE f_fail IS NULL ";
		else if($status == 'failed')
			$sql .= "WHERE f_fail =1 ";
		
		if($date != NULL && $status != NULL)
			$sql .= "AND `f_datetime` >= '".$date." 00:00:00' AND `f_datetime` <= '".$date." 23:59:59' ";
		else if($date != NULL && $status == NULL)
			$sql .= "WHERE `f_datetime` >= '".$date." 00:00:00' AND `f_datetime` <= '".$date." 23:59:59' ";

		$sql .= "GROUP BY ip ORDER BY count( * ) DESC LIMIT 0 ,".$limit;
		//echo $sql;
		return $wpdb->get_results( $sql );
		
	}
	/*
	 * SELECT count( * ) , `f_url`
FROM `wfew_downloadchibashi_files_list`
GROUP BY `f_url`
ORDER BY count( * ) DESC
LIMIT 0 , 5
	 */
	private function top_link_download($date=NULL , $status=NULL , $limit=5 )
	{
		global $wpdb;
		$sql = "SELECT count( * ) AS Count , f_url FROM {$wpdb->prefix}downloadchibashi_files_list WHERE `f_url` != '' ";		
		
		if($status == 'success')
			$sql .= "AND f_fail IS NULL ";
		else if($status == 'failed')
			$sql .= "AND f_fail =1 ";
		
		if($date != NULL)
			$sql .= "AND `f_datetime` >= '".$date." 00:00:00' AND `f_datetime` <= '".$date." 23:59:59' ";

		$sql .= "GROUP BY f_url ORDER BY count( * ) DESC LIMIT 0 ,".$limit;
		//echo $sql;
		return $wpdb->get_results( $sql );
		
	}
    private function download_statistics_chart_data($date = NULL)
    {
        global $wpdb;
               
		for($i =0;$i<12 ;$i++)
		{
			$result['failed'][$i] = $result['success'][$i] = 0;
		}
		
        date_default_timezone_set('Asia/Tehran');

        if($date == NULL)
            $date = date("Y-m-d");

        $date_sp = explode('-', $date);
        for($i=0;$i<12;$i++)
            $result['date'][$i] = date("Y-m-d",mktime(0,0,0,$date_sp[1],$date_sp[2]-$i,$date_sp[0]));
        
        $result['date'] = array_reverse($result['date']);

		$sql = "select count(*) AS Count,f_datetime from {$wpdb->prefix}downloadchibashi_files_list where f_fail =1 ";
		
		if($date != NULL)
			$sql .= "AND f_datetime <= '".$date."  23:59:59' ";
		$sql .= "group by DATE(f_datetime) ORDER BY f_datetime DESC LIMIT 0 , 12";

        $list = $wpdb->get_results($sql);	
        foreach ($list as $list_row )
        {
            $db_date = explode( ' ',$list_row->f_datetime);

            if(in_array($db_date[0], $result['date']))          
                $result['failed'][array_search($db_date[0], $result['date'])] = (int)$list_row->Count;
            
        }        		
		$sql = "select count(*) AS Count,f_datetime from {$wpdb->prefix}downloadchibashi_files_list where f_fail IS NULL ";
		
		if($date != NULL)
			$sql .= "AND f_datetime <= '".$date."  23:59:59' ";
		$sql .= "group by DATE(f_datetime) ORDER BY f_datetime DESC LIMIT 0 , 12";
		
		
		$list = $wpdb->get_results($sql);	
        foreach ($list as $list_row )
        {
            $db_date = explode( ' ',$list_row->f_datetime);

            if(in_array($db_date[0],$result['date']))
                $result['success'][array_search($db_date[0],$result['date'])] = (int)$list_row->Count;
        }

        return $result;
        //return $Date_char;
    }
    
    private function plugin_working_time()
    {
        global $wpdb;
        $sql = "select min(f_datetime) AS date from {$wpdb->prefix}downloadchibashi_files_list ";
        
        $result = $wpdb->get_results($sql);
        foreach ($result as $result_row )
                $db_date = explode( ' ',$result_row->date);

        $first_date   = date_create($db_date[0]);
        $current_date = date_create(date("Y-m-d"));
        $diff=date_diff($first_date,$current_date);
        return $diff->format("%a");
    }
	/**
	 * Plugin settings page
	 */
	public function plugin_settings_page() 
    {	
        $url = $this->plugin_url();
        if(empty( $_REQUEST['status'] ))
        {
            $download_status = $this->user_download_status();
            $working_time = $this->plugin_working_time();
        }
        include("theme/index.php");
	}  
    /**
    * plugin url
    */
    private function plugin_url()
    {
        $url = $_SERVER['SERVER_NAME'] . dirname(__FILE__);
		$array = explode('/',$url);
		$count = count($array);
		$pdn = $array[$count-1];
		$url = get_option('home')."/wp-content/plugins/$pdn/";
        return $url;   
    }
    /*
	 * Dashbord statistics chart  
	 */
	public function dashbord_statistics_chart()
	{
		$url = $this->plugin_url();	
		$char_date = $this -> download_statistics_chart_data();
		include_once ("theme/chart.php");		
	}	
	/**
	 * Screen options
	 */
	public function screen_option() {

		$option = 'per_page';
		$args   = [
			'label'   => 'Downloads status',
			'default' => 5,
			'option'  => 'downloads_per_page'
		];

		add_screen_option( $option, $args );

		$this->downloads_obj = new Download_Status();
	}


	/** Singleton instance */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

}

add_action( 'plugins_loaded', function () {
	Download_Status_Plugin::get_instance();
} );


// Function that outputs the contents of the dashboard widget
function dashboard_chart_widget_function( $post, $callback_args ) {
    $download_status = new Download_Status_Plugin();
    $char_date = $download_status -> dashbord_statistics_chart();
}

// Function used in the action hook
function add_dashboard_chart_widgets() {
	wp_add_dashboard_widget('dashboard_widget', 'نمودار دانلود', 'dashboard_chart_widget_function');
}

// Register the new dashboard widget with the 'wp_dashboard_setup' action
add_action('wp_dashboard_setup', 'add_dashboard_chart_widgets' );

