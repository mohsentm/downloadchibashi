<link href="<?php echo $url?>theme/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo $url?>theme/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
<style>
.nav-tabs > li {
    float: right;
}
.col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9 {
    float: right;
direction: rtl;
}
</style>
<div class="wrap">
    <ul class="nav nav-tabs">
      <li role="presentation" <?php if(empty( $_REQUEST['status'] )) echo 'class="active"'?>><a href="<?php echo get_option('home'); ?>/wp-admin/admin.php?page=download_status">آمار</a></li>
      <li role="presentation" <?php if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "all" ) echo 'class="active"'?>><a href="<?php echo get_option('home'); ?>/wp-admin/admin.php?page=download_status&status=all">کل دانلود ها</a></li>
      <li role="presentation" <?php if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "success" ) echo 'class="active"'?>><a href="<?php echo get_option('home'); ?>/wp-admin/admin.php?page=download_status&status=success">دانلود های موفق</a></li>
      <li role="presentation" <?php if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "failed" ) echo 'class="active"'?>><a href="<?php echo get_option('home'); ?>/wp-admin/admin.php?page=download_status&status=failed">دانلود های ناموفق</a></li>
    </ul>
<?php

    if(empty( $_REQUEST['status'] ))
        include_once("status.php");
    
    else if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "all" )
        include_once("all.php");

    else if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "success" )
        include_once("success.php");

    else if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "failed" )
        include_once("failed.php");



?>
</div>
