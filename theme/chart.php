<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<style type="text/css">
${demo.css}
		</style>
		<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        title: {
            text: 'نمودار دانلود',
            x: 0 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: <?php echo json_encode($char_date['date']);  ?>
        },
        yAxis: {
            title: {
                text: 'تعداد دانلود'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [ {
            name: 'موفق',
            data: <?php echo json_encode($char_date['success']);  ?>
        }, {
            name: 'ناموفق',
            data: <?php echo json_encode($char_date['failed']);  ?>
        }]
    });
});

	</script>
	</head>
	<body>
<script src="<?php echo $url?>theme/js/highcharts.js"></script>
<script src="<?php echo $url?>theme/js/exporting.js"></script>

<div id="container" style="direction: ltr;"></div>
