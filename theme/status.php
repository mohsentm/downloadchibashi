<?php
if (isset($_POST['chart_date']))
	$char_date = $this -> download_statistics_chart_data($_POST['chart_date']);
else
	$char_date = $this -> download_statistics_chart_data();
?>	
<div class="wrap">
    <div id="poststuff" style="direction: ltr;"> 
        <div id="post-body" class="metabox-holder">
            <div id="post-body-content">
                <div class="row">
                    <div class="col-md-4">
						<div class="panel panel-info">
						  <div class="panel-heading">آمار کلی</div>
						  	<table class="table">
								<tbody>
									<tr>
										<td>کار کرد پلاگین :</td>
										<td><?php echo $working_time; ?></td>
										<td>روز</td>
									</tr>
									<tr>
										<td>کل دانلود های موفق : </td>
										<td><?php echo $download_status['success']; ?></td>
										<td>دانلود</td>
									</tr>
									<tr>
										<td>کل دانلود های ناموفق  :</td>
										<td><?php echo $download_status['fail']; ?></td>
										<td>دانلود</td>
									</tr>			
									<tr>
										<td>تعداد دانلود های موفق امروز :</td>
										<td><?php echo end($char_date['success']) ?></td>
										<td>دانلود</td>
									</tr>
									<tr>
										<td>تعداد دانلود های ناموفق امروز :</td>
										<td><?php echo end($char_date['failed']) ?></td>
										<td>دانلود</td>
									</tr>
								</tbody>
							</table>
						</div>
						
						<div class="panel panel-success">
						  <div class="panel-heading">ای پی های برتر</div>
						  	<table class="table">
								<tbody>
								<?php 
									$top_ip_list = $this -> top_user_download_ip();									
									foreach($top_ip_list as $top_ip_list_row):
								?>	
									<tr>										
										<td><?php echo $top_ip_list_row->ip; ?></td>
										<td><?php echo $top_ip_list_row->Count; ?></td>
										<td>دانلود</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
						</div>
                    </div>
                    <div class="col-md-8">
                        <form method="post">
                        <input type="date" name="chart_date" value="<?php if(isset($_POST['chart_date'])) echo $_POST['chart_date']; else echo date("Y-m-d") ?>">
                        <input type="submit" value="فیلتر">
                        </form>
                        <br>
                        <?php
						    include_once ("chart.php");
                        ?> 
                    </div>
                </div>                                            
            	<div class="row">
            		<div class="col-md-3">
						<div class="panel panel-success">
						  <div class="panel-heading">ای پی بیشترین دانلود موفق امروز</div>
						  	<table class="table">
								<tbody>
								<?php 
									$top_ip_list = $this -> top_user_download_ip(date("Y-m-d"),"success");									
									foreach($top_ip_list as $top_ip_list_row):
								?>	
									<tr>										
										<td><?php echo $top_ip_list_row->ip; ?></td>
										<td><?php echo $top_ip_list_row->Count; ?></td>
										<td>دانلود</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
						</div>
                    </div>
                    <div class="col-md-3">
						<div class="panel panel-danger">
						  <div class="panel-heading">ای پی بیشترین دانلود ناموفق امروز</div>
						  	<table class="table">
								<tbody>
								<?php 
									$top_ip_list = $this -> top_user_download_ip(date("Y-m-d"),"failed");									
									foreach($top_ip_list as $top_ip_list_row):
								?>	
									<tr>										
										<td><?php echo $top_ip_list_row->ip; ?></td>
										<td><?php echo $top_ip_list_row->Count; ?></td>
										<td>دانلود</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
						</div>
                    </div>
                    <div class="col-md-3">
						<div class="panel panel-success">
						  <div class="panel-heading">ای پی بیشترین دانلود موفق </div>
						  	<table class="table">
								<tbody>
								<?php 
									$top_ip_list = $this -> top_user_download_ip("","success");									
									foreach($top_ip_list as $top_ip_list_row):
								?>	
									<tr>										
										<td><?php echo $top_ip_list_row->ip; ?></td>
										<td><?php echo $top_ip_list_row->Count; ?></td>
										<td>دانلود</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
						</div>
                    </div>
                    <div class="col-md-3">
						<div class="panel panel-danger">
						  <div class="panel-heading">ای پی بیشترین دانلود ناموفق</div>
						  	<table class="table">
								<tbody>
								<?php 
									$top_ip_list = $this -> top_user_download_ip("","failed");									
									foreach($top_ip_list as $top_ip_list_row):
								?>	
									<tr>										
										<td><?php echo $top_ip_list_row->ip; ?></td>
										<td><?php echo $top_ip_list_row->Count; ?></td>
										<td>دانلود</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
						</div>
                    </div>	            		
            	</div>
            	<div class="row">
            		<div class="col-md-6">
						<div class="panel panel-success">
						  <div class="panel-heading">بیشتر آدرس دانلود موفق امروز</div>
						  	<table class="table">
								<tbody>
								<?php 
									$top_link_list = $this -> top_link_download(date("Y-m-d"),"success");									
									foreach($top_link_list as $top_link_list_row):
								?>	
									<tr>										
										<td><?php echo $top_link_list_row->f_url; ?></td>
										<td><?php echo $top_link_list_row->Count; ?></td>
										<td>دانلود</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
						</div>
                    </div>
                    <div class="col-md-6">
						<div class="panel panel-danger">
						  <div class="panel-heading">بیشترین آدرس دانلود ناموفق امروز</div>
						  	<table class="table">
								<tbody>
								<?php 
									$top_link_list = $this -> top_link_download(date("Y-m-d"),"failed");									
									foreach($top_link_list as $top_link_list_row):
								?>	
									<tr>										
										<td><?php echo $top_link_list_row->f_url; ?></td>
										<td><?php echo $top_link_list_row->Count; ?></td>
										<td>دانلود</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
						</div>
                    </div>
                </div>
            	<div class="row">
                    <div class="col-md-6">
						<div class="panel panel-success">
						  <div class="panel-heading">بیشترین آدرس دانلود موفق</div>
						  	<table class="table">
								<tbody>
								<?php 
									$top_link_list = $this -> top_link_download("","success");									
									foreach($top_link_list as $top_link_list_row):
								?>	
									<tr>										
										<td><?php echo $top_link_list_row->f_url; ?></td>
										<td><?php echo $top_link_list_row->Count; ?></td>
										<td>دانلود</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
						</div>
                    </div>
                    <div class="col-md-6">
						<div class="panel panel-danger">
						  <div class="panel-heading">بیشترین آدرس دانلود ناموفق</div>
						  	<table class="table">
								<tbody>
								<?php 
									$top_link_list = $this -> top_link_download("","failed");									
									foreach($top_link_list as $top_link_list_row):
								?>	
									<tr>										
										<td><?php echo $top_link_list_row->f_url; ?></td>
										<td><?php echo $top_link_list_row->Count; ?></td>
										<td>دانلود</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
							<div class="panel-footer">
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">بیشتر</button>
							</div>
						</div>
                    </div>                    	            	
            	</div>            
            </div>
        </div>
        <br class="clear">
    </div>
</div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<h4 class="modal-title" id="myLargeModalLabel">Large modal</h4>
		</div>
		<div class="modal-body">
			...
		</div>    	      
    </div>
  </div>
</div>


