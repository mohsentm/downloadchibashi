<?php

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Download_Status extends WP_List_Table {

	/** Class constructor */
	public function __construct() {

		parent::__construct( [
			'singular' => __( 'Download', 'sp' ), //singular name of the listed records
			'plural'   => __( 'Downloads', 'sp' ), //plural name of the listed records
			'ajax'     => false //does this table support ajax?
		] );

	}


	/**
	 * Retrieve downloads data from the database
	 *
	 * @param int $per_page
	 * @param int $page_number
	 *
	 * @return mixed
	 */
/*
تعداد دانلود های موفق به تفکیک روز
 select count(*),f_datetime from  wp_downloadchibashi_files_list group by DATE(f_datetime)
تعداد دانلود های ناموفق با تفکیک روز
select count(*),f_datetime  from  wp_downloadchibashi_files_list where f_fail =1 group by DATE(f_datetime);
*/
	public static function get_downloads( $per_page = 5, $page_number = 1 ) {

		global $wpdb;

        if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "all" )
		{
		    $sql = "select * from  {$wpdb->prefix}downloadchibashi_files_list";
			if ( ! empty( $_REQUEST['s'] ) ) {
				 $search = trim($_REQUEST['s']);
				 $sql .= " WHERE ip = '%$search%' OR f_url LIKE '%$search%' OR (`f_datetime` >= '$search 00:00:00' AND `f_datetime` <= '$search 23:59:59' )";
			}
		}
        else if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "success" )        
			$sql = "select count(*),f_datetime  from  {$wpdb->prefix}downloadchibashi_files_list where f_fail IS NULL group by DATE(f_datetime)";

        else if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "failed" )
		    $sql = "select count(*),f_datetime from  {$wpdb->prefix}downloadchibashi_files_list where f_fail =1 group by DATE(f_datetime)";

		if ( ! empty( $_REQUEST['orderby'] ) ) {
			$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
		}
        else
		{
			$sql .= ' ORDER BY f_datetime DESC';
		}
		$sql .= " LIMIT $per_page";
		$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		return $result;
	}

	/**
	 * Delete a download record.
	 *
	 * @param int $id download ID
	 */
	public static function delete_download( $id ) {
		global $wpdb;
        
        if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "all" )
		    $wpdb->delete(
			    "{$wpdb->prefix}downloadchibashi_files_list",
			    [ 'f_id' => $id ],
			    [ '%d' ]
		    );
        else if(!empty( $_REQUEST['status']) && ($_REQUEST['status'] == "success"  ||  $_REQUEST['status'] == "failed" )) 
            $wpdb->delete(
			    "{$wpdb->prefix}downloadchibashi_files_list",
			    [ 'f_datetime' => $id ],
			    [ '%d' ]
		    );
        
	}


	/**
	 * Returns the count of records in the database.
	 *
	 * @return null|string
	 */

	public static function record_count() {
		global $wpdb;
		
        if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "all" )
		    $sql = "select count(*) from  {$wpdb->prefix}downloadchibashi_files_list ";

        else if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "success" )        
		    $sql = "select count(*) from  {$wpdb->prefix}downloadchibashi_files_list where f_fail IS NULL group by DATE(f_datetime)";

        else if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "failed" )
		    $sql = "select count(*) from  {$wpdb->prefix}downloadchibashi_files_list where f_fail =1 group by DATE(f_datetime)";

		return $wpdb->get_var( $sql );
	}


	/** Text displayed when no download data is available */
	public function no_items() {
		_e( 'No downloads avaliable.', 'sp' );
	}


	/**
	 * Render a column when no column specific method exist.
	 *
	 * @param array $item
	 * @param string $column_name
	 *
	 * @return mixed
	 */

	public function column_default( $item, $column_name ) {

    if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "all" )
        switch ( $column_name ) {
			case 'f_id':
			case 'ip':
            case 'f_url':
            case 'f_fail ':
            case 'f_datetime':
				return $item[ $column_name ];
			default:
				return print_r( $item, true ); //Show the whole array for troubleshooting purposes
		}

    else if(!empty( $_REQUEST['status']) && ($_REQUEST['status'] == "success"  ||  $_REQUEST['status'] == "failed" )) 
        switch ( $column_name ) {
			case 'f_datetime':
            case 'count(*)':
				return $item[ $column_name ];
			default:
				return print_r( $item, true ); //Show the whole array for troubleshooting purposes
		}

		
	}

	/**
	 * Render the bulk edit checkbox
	 *
	 * @param array $item
	 *
	 * @return string
	 */
	function column_cb( $item ) {
        if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "all" )
		    return sprintf(
			    '<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['f_id']
		    );
        else if(!empty( $_REQUEST['status']) && ($_REQUEST['status'] == "success"  ||  $_REQUEST['status'] == "failed" )) 
            return sprintf(
			    '<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['f_datetime']
		    );
	}

	function column_fil_status( $item ) {

		if($item['f_fail'] == 1)
		{
			return sprintf( 'failed' );
		}
		else
			return sprintf( 'successful' );
        //return $item['count(*)'] ;
	}

	/**
	 * Method for name column
	 *
	 * @param array $item an array of DB data
	 *
	 * @return string
	 */
	function column_name( $item ) {

		$delete_nonce = wp_create_nonce( 'sp_delete_download' );

        if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "all" )
        {
		    $title = '<strong>' . $item['ip'] . '</strong>';
            
		    $actions = [
			    'delete' => sprintf( '<a href="?page=%s&action=%s&download=%s&_wpnonce=%s">Delete</a>', esc_attr( $_REQUEST['page'] ), 'delete', absint( $item['f_id'] ), $delete_nonce )
		    ];
        }
        else if(!empty( $_REQUEST['status']) && ($_REQUEST['status'] == "success"  ||  $_REQUEST['status'] == "failed" )) 
        {
		    $title = '<strong>' . $item['f_datetime'] . '</strong>';
            
		    $actions = [
			    'delete' => sprintf( '<a href="?page=%s&action=%s&download=%s&_wpnonce=%s">Delete</a>', esc_attr( $_REQUEST['page'] ), 'delete', absint( $item['f_datetime'] ), $delete_nonce )
		    ];
        }
		return $title . $this->row_actions( $actions );
	}


	/**
	 *  Associative array of columns
	 *
	 * @return array
	 */

	function get_columns() {
        
        if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "all" )
		    $columns = [
			    'cb'      => '<input type="checkbox" />',
			    'ip'    => __( 'IP Address', 'sp' ),
                'f_datetime' => __( 'Date&Time', 'sp' ),
			    'f_url' => __( 'Url', 'sp' ),
			    'fil_status'    =>  'Status'
		    ];
        else if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "success") 
		    $columns = [
			    'cb'      => '<input type="checkbox" />',
                'f_datetime' => __( 'Date&Time', 'sp' ),
			    'count(*)' => __( 'Download Success', 'sp' )
		    ];
        else if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "failed" )
		    $columns = [
			    'cb'      => '<input type="checkbox" />',
                'f_datetime' => __( 'Date&Time', 'sp' ),
			    'count(*)' => __( 'Download Failed', 'sp' )
		    ];
		return $columns;
	}


	/**
	 * Columns to make sortable.
	 *
	 * @return array
	 */

	public function get_sortable_columns() {
		
        if(!empty( $_REQUEST['status']) && $_REQUEST['status'] == "all" )
		    $sortable_columns = array(
			'ip' => array( 'ip', true ),
			'f_datetime' => array( 'f_datetime', false ),
            'f_url' => array( 'f_url', false ),
            'f_fail' => array( 'f_fail', false )
		    );
        else if(!empty( $_REQUEST['status']) && ($_REQUEST['status'] == "success"  ||  $_REQUEST['status'] == "failed" )) 
		    $sortable_columns = array(
			'f_datetime' => array( 'f_datetime', false ),
            'count(*)' => array( 'f_url', false ),
		    );
		return $sortable_columns;
	}

	/**
	 * Returns an associative array containing the bulk action
	 *
	 * @return array
	 */
	public function get_bulk_actions() {
		$actions = [
			'bulk-delete' => 'حذف'
		];

		return $actions;
	}


	/**
	 * Handles data query and filter, sorting, and pagination.
	 */
	public function prepare_items() {

		$this->_column_headers = $this->get_column_info();

		/** Process bulk action */
		$this->process_bulk_action();

		$per_page     = $this->get_items_per_page( 'downloads_per_page', 20 );
		$current_page = $this->get_pagenum();
		$total_items  = self::record_count();

		$this->set_pagination_args( [
			'total_items' => $total_items, //WE have to calculate the total number of items
			'per_page'    => $per_page //WE have to determine how many items to show on a page
		] );

		$this->items = self::get_downloads( $per_page, $current_page );
	}

	public function process_bulk_action() {

		//Detect when a bulk action is being triggered...
		if ( 'delete' === $this->current_action() ) {

			// In our file that handles the request, verify the nonce.
			$nonce = esc_attr( $_REQUEST['_wpnonce'] );

			if ( ! wp_verify_nonce( $nonce, 'sp_delete_download' ) ) {
				die( 'Go get a life script kiddies' );
			}
			else {
				self::delete_download( absint( $_GET['download'] ) );

				wp_redirect( esc_url( add_query_arg() ) );
				exit;
			}

		}

		// If the delete bulk action is triggered
		if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete' )
		     || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' )
		) {

			$delete_ids = esc_sql( $_POST['bulk-delete'] );

			// loop over the array of record IDs and delete them
			foreach ( $delete_ids as $id ) {
				self::delete_download( $id );

			}

			//wp_redirect( esc_url( add_query_arg() ) );
			//exit;
		}
	}

}
?>
